# Terraform-aks

"""
Este repositorio contiene la infraestructura de un clúster de Azure Kubernetes Service (AKS) creado utilizando Terraform. El código incluye la creación del clúster, el grupo de recursos y las configuraciones básicas de red.

Para utilizar este repositorio y crear la infraestructura de AKS, sigue los siguientes pasos:

1. Inicia sesión en Azure: 
    - Abre una terminal y ejecuta el comando `az login` para iniciar sesión en tu cuenta de Azure.

2. Configura las variables de entorno(Opcional):
    - Abre el archivo `variables.tf` y actualiza las variables según tus necesidades. Estas variables incluyen el nombre del clúster, la ubicación, el tamaño de las instancias, etc.

3. Inicializa el backend de Terraform:
    - Ejecuta el comando `terraform init` para inicializar el backend de Terraform y descargar los proveedores necesarios.

4. Planifica los cambios:
    - Ejecuta el comando `terraform plan` para ver los cambios que se realizarán en la infraestructura.

5. Aplica los cambios:
    - Ejecuta el comando `terraform apply` para crear la infraestructura de AKS en Azure.

Una vez que hayas completado estos pasos, tendrás un clúster de AKS completamente funcional.

A continuación, se proporcionará una sección adicional para explicar cómo instalar Argo CD en el clúster de AKS.

# Instalación manual de Argo CD en el clúster de AKS

Para instalar Argo CD en el clúster de AKS, sigue los siguientes pasos:

1. Crea un espacio de nombres para Argo CD ejecutando el siguiente comando:
    ```
    kubectl create namespace argocd
    ```

2. Aplica el archivo de instalación de Argo CD ejecutando el siguiente comando:
    ```
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```

3. Actualiza el servicio de Argo CD para que utilice un balanceador de carga ejecutando el siguiente comando:
    ```
    kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
    ```

4. Obtén la contraseña inicial del usuario administrador de Argo CD ejecutando el siguiente comando:
    ```
    kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
    ```

Una vez que hayas completado estos pasos, Argo CD estará instalado y configurado en tu clúster de AKS. Puedes acceder a la interfaz de Argo CD utilizando la dirección IP del balanceador de carga y la contraseña obtenida en el paso anterior.

"""


